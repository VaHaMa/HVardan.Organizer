﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Web;
using Systrotech.Organizer.Repository.Entities;
using Systrotech.Organizer.ViewModels;

namespace Systrotech.Organizer.Mappers
{
    public class Mapper
    {
        public static User ToRegisterViewModel(AccountViewModel model)
        {

            User user = new User();
            Category category = new Category();
            SHA1 sha1ob = SHA1.Create();

            if (model.CategoryIdLevel1 == 1)
            {
                category.CategoryId = 1;
                user.Experience = "";
                user.Picture_Link = "";
            }
            else
            {
                //string esim = model.CategoryIdLevel2;
                category.CategoryId = Convert.ToInt32(model.CategoryIdLevel2);
                user.Experience = model.Experience;
                user.Picture_Link = model.Picture_Link;
            }

            user.Name = model.Name;
            user.Surname = model.SurName;
            user.UserName = model.Username;
            user.Password = Helper.Helper.GetMd5Hash(sha1ob, model.Password);
            user.Email = model.Email;
            user.CategoryId = category.CategoryId;

            return user;

        }
    }
}