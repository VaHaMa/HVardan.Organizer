﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using Systrotech.Organizer.Mappers;
using Systrotech.Organizer.Repository.Entities;
using Systrotech.Organizer.Repository.Services;
using Systrotech.Organizer.ViewModels;

namespace Systrotech.Organizer.Controllers
{
    public class AccountController : Controller
    {
        public OrganizerEntities organizerEntities = new OrganizerEntities();

        private IOrganizerRepository repository;
        public AccountController()
        {
            repository = new OrganizerRepository();
        }


        public ActionResult Login()
        {
            LoginViewModel account = checkCookie();
            if (account == null)
                return View();

            else
            {
                LoginViewModel loginModel = new LoginViewModel();
                if (loginModel.login(account.Username, account.Password))
                {
                    Session["username"] = account.Username;
                    return RedirectToAction("Index", "Home");

                }
                else
                {
                    ViewBag.Error = "Account's Invalid";
                    return View();
                }

            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Login(LoginViewModel model)
        {

            if (model.login(model.Username, model.Password))
            {
                Session["username"] = model.Username;
                if (model.remember)
                {
                    HttpCookie ckUsername = new HttpCookie("username");
                    ckUsername.Expires = DateTime.Now.AddSeconds(3600);
                    ckUsername.Value = model.Username;
                    Response.Cookies.Add(ckUsername);

                    HttpCookie ckPassword = new HttpCookie("password");
                    ckPassword.Expires = DateTime.Now.AddSeconds(3600);
                    ckPassword.Value = model.Password;
                    Response.Cookies.Add(ckPassword);
                }
                return RedirectToAction("Index", "Home");
            }
            else
            {
                ViewBag.Error = "Account's Invalid";
                return View();
            }
        }



        public LoginViewModel checkCookie()
        {

            LoginViewModel user = null;
            string username = string.Empty, password = string.Empty;
            if (Request.Cookies["username"] != null)
                username = Request.Cookies["username"].Value;
            if (Request.Cookies["password"] != null)
                password = Request.Cookies["password"].Value;
            if (!string.IsNullOrEmpty(username) && !string.IsNullOrEmpty(password))
                user = new LoginViewModel { Username = username, Password = password };

            return user;

        }


        public ActionResult Logout()
        {
            //Remove.Sesion
            Session.Remove("username");

            // Remove.Cookie
            if (Response.Cookies["username"] != null)
            {
                HttpCookie ckUsername = new HttpCookie("username");
                ckUsername.Expires = DateTime.Now.AddDays(-1d);
                Response.Cookies.Add(ckUsername);
            }

            if (Response.Cookies["password"] != null)
            {
                HttpCookie ckPassword = new HttpCookie("password");
                ckPassword.Expires = DateTime.Now.AddDays(-1d);
                Response.Cookies.Add(ckPassword);
            }

            return RedirectToAction("Login", "Account");

        }












        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult CreateAccount(AccountViewModel model)
        {
            if (ModelState.IsValid)
            {
                var user = Mapper.ToRegisterViewModel(model);
                //user.CategoryId = (int)model.CategoryIdLevel2;
                repository.Insert(user);
                repository.Save();
                return RedirectToAction("Login", "Account");
            }
            else
            {
                return RedirectToAction("CreateAccount", "Account");
            }

        }

        public ActionResult CreateAccount()
        {
            AccountViewModel model = new AccountViewModel();
            model.MenuLevel1 = organizerEntities.Categories.Where(menu => menu.ParentId == null).
            ToList().Select(menu => new SelectListItem
            {
                Value = menu.CategoryId.ToString(),
                Text = menu.Name
            }).ToList();


            model.MenuLevel2 = new List<SelectListItem>();
            return View(model);
        }

        public ActionResult FilterCatLevel2(int id)
        {
            return Json(organizerEntities.Categories
                .Where(c => c.ParentId == id)
                .ToList().Select(c => new SelectListItem

                {

                    Value = c.CategoryId.ToString(),
                    Text = c.Name
                }).ToList(), JsonRequestBehavior.AllowGet);
        }
    }
}