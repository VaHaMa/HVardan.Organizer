﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Systrotech.Organizer.Repository.Entities;

namespace Systrotech.Organizer.Repository.Services
{
    public class OrganizerRepository : IOrganizerRepository
    {
        protected OrganizerEntities context;

        public OrganizerRepository()
        {
            context = new OrganizerEntities();
        }

        public void Insert<TModel>(TModel model) where TModel : class
        {
            context.Set<TModel>().Add(model);
        }

        public IEnumerable<TModel> SelectAll<TModel>() where TModel : class
        {
            return context.Set<TModel>().ToList();
        }

        public void Save()
        {
            context.SaveChanges();
        }


        #region IDisposable

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

     
        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (context != null)
                {
                    context.Dispose();
                    context = null;
                }
            }
        }


        #endregion
    }
}
