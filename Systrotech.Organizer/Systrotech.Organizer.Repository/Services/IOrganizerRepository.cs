﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Systrotech.Organizer.Repository.Services
{
    public interface IOrganizerRepository : IDisposable
    {
        IEnumerable<TModel> SelectAll<TModel>() where TModel : class;

        void Insert<TModel>(TModel model) where TModel : class;

        void Save();
    }
}
